from EmailClassifier import *
import sys
import os.path
import random

# get all the emails in the data set
docs = [Document(fileName) for fileName in os.listdir(DATASET_PATH)]

# create a new classifier, given the documents
classifier = EmailClassifier(docs)
# write the body and subject.csv
classifier.writeToFile()

# run naive bayes with 10 fold cross validation on the body.csv file
print "Body Corpus"
classifier.naiveBayes('body.csv')

# run naive bayes with 10 fold cross validation on the subject.csv file
print "\nSubject Corpus"
classifier.naiveBayes('subject.csv')

#classifier.printStdDevAndMean('body.csv')
#classifier.printStdDevAndMean('subject.csv')



