# split a list into approximately equal subsets, if equal subsets are not possible
def splitList(pList, numSets):
    #random.shuffle(pList)
    numPerSet = len(pList)/numSets
    splitList = [pList[i:i+numPerSet] for i in range(0, len(pList), numPerSet)]

    if len(splitList) > numSets:
        lastIndex = len(splitList)-1
        for i in range(len(splitList[lastIndex])):
                splitList[i].append(splitList[lastIndex][i])
        splitList.pop()
    else:
        return splitList
    return splitList
