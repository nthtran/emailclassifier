import math
# compute mean, given a column of data
def computeMean(column):
    column = [float(x) for x in column]
    return sum(column)/len(column)
# compute std dev, given a column of data
def computeStdDev(column):
    n = len(column)
    column = [float(x) for x in column]
    mean = computeMean(column)
    
    stdDev = (sum([(x - mean)**2 for x in column])/(n - 1))**0.5
    return stdDev

# compute the probability density, given a column of data and a value
def computeProbDensity(value, column):
    mean = computeMean(column)
    stdDev = computeStdDev(column)

    if stdDev == 0.0:
        stdDev = 0.0001 # return 0.0001 if standard deviation is 0 (otherwise division by 0 error)
    
    result = math.exp(-(value - mean)**2/(2*stdDev**2))/(stdDev*(2*math.pi)**0.5)
    return result
