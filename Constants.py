DATASET_PATH = 'dataset'
STOPWORDS_FILE = 'stop_words.txt'
FILE_EXTENSION = 'txt.gz'
BODY_FILE = 'body.csv'
SUBJECT_FILE = 'subject.csv'
STEMMING = False
