import sys
import os.path
import gzip
import re
from Constants import *
from stemming.porter2 import stem

class Document:
    def __init__(self, fileName):
        ## Variables ##
        self.name = fileName.rstrip('.' + FILE_EXTENSION) #(name of the email without the file extension)
        # a dict of corpora, their terms and term frequencies
        self.tf = {}

        ## Compute term frequencies ##
        # get a list of stop words
        stopWords = self.getStopWords()

        # open file
        doc = gzip.open(DATASET_PATH + "/" + fileName, 'r')
        # read the lines of the email
        content = doc.readlines()
        # strip "Subject:" from the first line
        content[0] = content[0].lstrip("Subject: ")

        # extract all words from the lines (in form of an array of arrays of words)
        lines = [re.findall(r'\w+|\!|\?|\$', line) for line in content] # including !, ? and $

        # count number of times a word appears in the subject, add it to subject corpus
        subject = {}
        for word in lines[0]:
            if subject.has_key(word):
                subject[word] += 1.0 # using float not in for more accurate results (not rounded)
            else:
                subject[word] = 1.0
        # count number of times a word appears in the body, add it to body corpus    
        body = {}
        for word in lines[2]:
            if body.has_key(word):
                body[word] += 1.0
            else:
                body[word] = 1.0

        self.tf['subject'] = {} # subject corpus
        self.tf['body'] = {} # body corpus

        # add the term to this email's corpora, if it is not a stop word
        for word, frequency in subject.iteritems():
            if word not in stopWords:
                self.tf['subject'][word] = frequency
                
        for word, frequency in body.iteritems():
            if word not in stopWords:
                self.tf['body'][word] = frequency

        if STEMMING == True:
            subject = {}
            body = {}

            for word, frequency in self.tf['subject'].iteritems():
                stemmed = stem(word)
                if subject.has_key(stemmed):
                    subject[stemmed] += frequency
                else:
                    subject[stemmed] = frequency
            for word, frequency in self.tf['body'].iteritems():
                stemmed = stem(word)
                if body.has_key(stemmed):
                    body[stemmed] += frequency
                else:
                    body[stemmed] = frequency

            self.tf['subject'] = subject
            self.tf['body'] = body
            

    # get the list of stop words
    def getStopWords(self):
        f = open(STOPWORDS_FILE, 'r')
        string = f.readline()
        list = string.split(',')
        return list

    # get the term frequency, given the term and the corpus
    def getTf(self, corpus, term):
        try:
            tf = self.tf[corpus][term]
        except KeyError:
            tf = 0.0 # return 0 if it doesn't exist
        return tf

    # check if this email has a given term
    def hasTerm(self, corpus, term):
        return self.tf[corpus].has_key(term)

    # get all the words in a given corpus
    def getWords(self, corpus):
        return self.tf[corpus]

    # get the name of this email
    def getName(self):
        return self.name
