import sys
import os.path
import gzip
import re
from collections import defaultdict
from time import time
import operator
from Constants import *
import csv
import math
from CSVReader import *
from Formulae import *
from Document import *
from Helpers import *
from stemming.porter2 import stem

# NOTE: the constants in this file are declared in Constants.py

class EmailClassifier:
    # Constructor, taking in a list of documents and number of features (top words), default number of features is 200
    def __init__(self, documents, numberOfWords = 200):
        self.documents = documents 
        self.df = {} # dict of terms and document frequencies (in each corpus)
        self.topWords = {} # dict of top words and their df (in each corpus)
        self.numberOfWords = numberOfWords
        
        self.df['subject'] = {}
        self.df['body'] = {}

        # for each document in the given list of documents     
        for doc in self.documents:
            # calculate the document frequency for each term in the subject corpus of this email
            for term in doc.getWords('subject').keys():
                if self.df['subject'].has_key(term):
                    self.df['subject'][term] += 1.0
                else:
                    self.df['subject'][term] = 1.0
            # calculate the document frequency for each term in the subject corpus of this email
            for term in doc.getWords('body').keys():
                if self.df['body'].has_key(term):
                    self.df['body'][term] += 1.0
                else:
                    self.df['body'][term] = 1.0

        # compute the lists of top words for the body and subject corpora        
        self.computeTopWords('body')
        self.computeTopWords('subject')

    # calculates returns the top words and their document frequencies, given the corpus
    def computeTopWords(self, corpus = 'body', descending = True):
        # sort the terms based on df, in descending order
        sortedWords = sorted(self.df[corpus].iteritems(), key=operator.itemgetter(1), reverse = descending)
        # get the top 200 words
        wordList = [sortedWords[i] for i in range(self.numberOfWords)]
        self.topWords[corpus] = wordList
        return self.topWords[corpus]

    # write the body.csv and subject.csv files
    def writeToFile(self):
        # getting the files to write
        if os.path.isfile(BODY_FILE):
            os.remove(BODY_FILE)
        if os.path.isfile(SUBJECT_FILE):
            os.remove(SUBJECT_FILE)
        bodyFile = open(BODY_FILE, 'wb')
        subjectFile = open(SUBJECT_FILE, 'wb')
        bodyWriter = csv.writer(bodyFile)
        subjectWriter = csv.writer(subjectFile)

        # write the headers (f1, f2, etc)
        row = ['f' + str(i+1) for i in range(self.numberOfWords)]
        row.append('IsSpam')
        #row.insert(0, 'Email')
        bodyWriter.writerow(row)
        subjectWriter.writerow(row)

        # write the important stuff
        # for each email in the given list
        for doc in self.documents:
            row = []
            # for each term in the top words of the body corpus
            for term in self.topWords['body']:
                # calculate the tfidf of the term
                row.append(self.normalisedTfidf(doc, term[0], 'body'))
            # check if the email is spam or non spam
            if doc.getName().startswith('spmsg'):
                row.append("True")
            else:
                row.append("False")
            # write the recorded data to file (body.csv)
            #row.insert(0, doc.getName())
            bodyWriter.writerow(row)
            row = []
            # for each term in the top words of the subject corpus
            for term in self.topWords['subject']:
                # calculate the tfidf of the term
                row.append(self.normalisedTfidf(doc, term[0], 'subject'))
            # check if the email is spam or non spam
            if doc.getName().startswith('spmsg'):
                row.append("True")
            else:
                row.append("False")
            # write the recorded data to file (subject.csv)
            #row.insert(0, doc.getName())
            subjectWriter.writerow(row)

        bodyFile.close()
        subjectFile.close()

    # calculate the idf of a term, given the corpus
    def getIdf(self, corpus, term):
        return math.log(len(self.documents)/(self.getDf(corpus, term) + 1)) # add 1 to the denominator to avoid division by 0
    
    # calculate the tf-idf
    def tfidf(self, document, term, corpus):
        return document.getTf(corpus, term) * self.getIdf(corpus, term)

    # normalise a given weight
    def normalise(self, weight, document, corpus):
        return weight/(sum([(self.tfidf(document, self.topWords[corpus][i][0], corpus))**2 for i in range(self.numberOfWords)])**0.5)

    # this is the main function (used in writing to the .csv files), calculating the normalised tf idf
    # other formula functions are used inside this function to calculate the final weight
    def normalisedTfidf(self, document, term, corpus):
        weight = self.tfidf(document, term, corpus)
        # only normalise if the weight is bigger than 0, if it's 0 then there's no need to
        if weight > 0:
            weight = self.normalise(weight, document, corpus)
        return weight

    # return the document frequency of a term, given the corpus
    def getDf(self, corpus, term):
        try:
            df = self.df[corpus][term]
        except KeyError:
            df = 0.0 # return 0 if it doesn't exist
        return df

    # get all the rows, spam or non spam in either the body or subject .csv file
    def getRows(self, corpus, isSpam):
        fileName = BODY_FILE if corpus == 'body' else SUBJECT_FILE
        reader = CSVReader(fileName)
    
        return [reader.getRow(i) for i in range(0, reader.getNumOfRows()) if reader.getValueAt(i, reader.getNumOfColumns()-1) == str(isSpam)]

    # get the column at the given index from all the given rows
    def getColumnFromRows(self, index, rows):
        return [row[index] for row in rows]

    # classify an instance (a row of features), given the training data (rows of spam emails and rows of non spam emails)
    def classify(self, doc, spamRows, hamRows):
        probESpam = []
        probEHam = [] # ham means not spam
        # get the probability that an email is spam
        probSpam = float(len(spamRows))/float(len(spamRows)+len(hamRows))
        # get the probability that an email is non spam
        probHam = float(len(hamRows))/float(len(spamRows)+len(hamRows))

        # for each feature
        for i in range(self.numberOfWords):
            column = i

            # get the column of all the spam rows (column of the feature)
            spamColumn = self.getColumnFromRows(column, spamRows)
            # calculate the probability density
            spamProbDensity = computeProbDensity(float(doc[column]), spamColumn)
            probESpam.append(spamProbDensity)
            
            # get the column of all the spam rows (column of the feature)
            hamColumn = self.getColumnFromRows(column, hamRows)
            # calculate the probability density
            hamProbDensity = computeProbDensity(float(doc[column]), hamColumn)
            probEHam.append(hamProbDensity)            
        # calculate the product of all prob density, spam and non spam
        productESpam = reduce(operator.mul, probESpam)
        productEHam = reduce(operator.mul, probEHam)

        # multiply the product with the prob that an email is spam 
        probSpamE = productESpam*probSpam
        # multiply the product with the prob that an email is non spam 
        probHamE = productEHam*probHam

        # compare the results, if probSpamE is greater or equal to probHamE, then the email is spam and vice versa        
        if probSpamE >= probHamE:
            return True
        else:
            return False

    # Naive bayes with 10 fold stratified cross validation, given a file name of the data (body.csv or subject.csv)
    def naiveBayes(self, fileName):
        # read the file 
        reader = CSVReader(fileName)
        # separate the data into spam emails and non spam emails
        spamDocs = [reader.getRow(i) for i in range(1, reader.getNumOfRows()) if reader.getValueAt(i, reader.getNumOfColumns()-1) == str(True)]
        hamDocs = [reader.getRow(i) for i in range(1, reader.getNumOfRows()) if reader.getValueAt(i, reader.getNumOfColumns()-1) == str(False)]

        # number of folds
        numSets = 10

        # split the spam and ham emails into 10 approximately equal subsets
        splitSpam = splitList(spamDocs, numSets)
        splitHam = splitList(hamDocs, numSets)

        # combine the subset from spam and non spam emails, ensuring equal proportions of spam and non spam emails in each subsets
        splitDocs = [splitSpam[i]+splitHam[i] for i in range(numSets)]
        
        # 10 fold cross validation
        sumAcc = []

        # for each subset
        for i in range(10):
            # training data is 9 subsets
            trainingSet = [doc for j in range(len(splitDocs)) if j != i for doc in splitDocs[j]]
            # testing data is the remaining subset
            testingSet = splitDocs[i]

            # split the training data into spam and non spam
            spamRows = [row for row in trainingSet if row[len(row)-1] == str(True)]
            hamRows = [row for row in trainingSet if row[len(row)-1] == str(False)]

            print "Run " + str(i+1)
            
            corrects = 0.0
            # for each email in the testing set
            for doc in testingSet:
                # actual result
                actResult = True if doc[len(doc)-1] == str(True) else False
                # classify it, given the training data (spam and non spam)
                result = self.classify(doc, spamRows, hamRows)
                # if the result is correct, increase the number of correct results
                if result == actResult:
                    corrects += 1.0
            # calculate the accuracy
            acc = corrects/len(testingSet)
            sumAcc.append(acc)
            print "    Accuracy: " + str(acc*100) + "%"

        print "Overall"
        print "    Average Accuracy: " + str((sum(sumAcc)/10)*100) + "%"

    # for testing, print std dev and mean
    def printStdDevAndMean(self, fileName):
        reader = CSVReader(fileName)
        spamDocs = [reader.getRow(i) for i in range(1, reader.getNumOfRows()) if reader.getValueAt(i, reader.getNumOfColumns()-1) == str(True)]
        hamDocs = [reader.getRow(i) for i in range(1, reader.getNumOfRows()) if reader.getValueAt(i, reader.getNumOfColumns()-1) == str(False)]                

        for i in range(self.numberOfWords):
            spamColumn = self.getColumnFromRows(i+1, spamDocs)            
            hamColumn = self.getColumnFromRows(i+1, hamDocs)

            print "f" + str(i+1) + " " + str(computeMean(hamColumn)) + " " + str(computeMean(spamColumn))
            print "f" + str(i+1) + " " + str(computeStdDev(hamColumn)) + " " + str(computeStdDev(spamColumn))
