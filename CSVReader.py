import csv

class CSVReader:
    def __init__(self, fileName):
        # file is a list of rows, a row is a list of values
        f = open(fileName, 'r')
        self.file = [row for row in csv.reader(f)]
        f.close()
        
    def getNumOfRows(self):
        return len(self.file)

    # assuming every row has the same num of columns
    def getNumOfColumns(self):
        return len(self.file[0])

    # return the column as a list
    def getColumn(self, index, fromRow = 0, toRow = None):
        if toRow == None:
            toRow = len(self.file)
        return [self.file[i][index] for i in range(fromRow, toRow+1)]

    # return the row as a list
    def getRow(self, index, fromColumn = 0, toColumn = None):
        if fromColumn == 0 and toColumn == None:
            return self.file[index]
        else:
            return [self.file[index][i] for i in range(fromColumn, toColumn+1)]

    def getValueAt(self, row, column):
        return self.file[row][column]
